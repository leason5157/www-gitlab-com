document.addEventListener("DOMContentLoaded", () => {
  trackProductImpressions();
  trackAddToCart();
});

const productInfo = [
  {
    name: "Premium",
    id: "0002",
    brand: "GitLab",
    category: "DevOps",
    price: "288",
  },
  {
    name: "Ultimate",
    id: "0001",
    price: "1188",
    brand: "GitLab",
    category: "DevOps",
  },
];

function trackProductImpressions() {
  if (window.dataLayer) {
    window.dataLayer.push({
      event: "EECproductView",
      ecommerce: {
        currencyCode: "USD",
        impressions: productInfo
      },
    });
  }
}

function trackAddToCart() {
  const premiumSaasButton = document.querySelector("[data-ga-ecomm='silver-premium-saas']");
  const premiumSelfHostedButton = document.querySelector("[data-ga-ecomm='silver-premium-hosted']");
  const ultimateSaasButton = document.querySelector("[data-ga-ecomm='gold-ultimate-saas']");
  const ultimateSelfHostedButton = document.querySelector("[data-ga-ecomm='gold-ultimate-hosted']");

  const premiumProduct = productInfo[0];
  const ultimateProduct = productInfo[1];

  const premiumSaasCartInfo = {
    ...premiumProduct,
    quantity: 1,
    variant: 'SaaS',
  }

  const premiumSelfHostedCartInfo = {
    ...premiumProduct,
    quantity: 1,
    variant: 'Self-Hosted',
  }

  const ultimateSaasCartInfo = {
    ...ultimateProduct,
    quantity: 1,
    variant: 'SaaS',
  }

  const ultimateSelfHostedCartInfo = {
    ...ultimateProduct,
    quantity: 1,
    variant: 'Self-Hosted',
  }

  premiumSaasButton.addEventListener("click", () => { handleAddToCart(premiumSaasCartInfo) });
  premiumSelfHostedButton.addEventListener("click", () => { handleAddToCart(premiumSelfHostedCartInfo) });
  ultimateSaasButton.addEventListener("click", () => { handleAddToCart(ultimateSaasCartInfo) });
  ultimateSelfHostedButton.addEventListener("click", () => { handleAddToCart(ultimateSelfHostedCartInfo) });
}

function handleAddToCart(product) {
  if (window.dataLayer) {
    window.dataLayer.push({
      event: "EECproductAddToCart",
      ecommerce: {
        currencyCode: "USD",
        add: {
          products: [product]
        }
      },
    });
  }
}